# jekyll-kde-theme

This is a jekyll theme for a unified layout across the kde websites.

## Installation

To create a new applications, copy the directory `examples/applications`

And replace

```ruby
gem "jekyll-kde-theme", path: '../../'
```

with

```ruby
gem "jekyll-kde-theme", :git => 'https://invent.kde.org/websites/jekyll-kde-theme.git'
```

And then execute:

    $ bundle install --path vendor/bundle

You can now start editing the website.

To test your change do:

    $ bundle exec jekyll server

You can also find a tutorial in the community wiki: https://community.kde.org/KDE.org/Jekyll

## Usage

### Collections:

* Changelogs (optional): put the software changelog in \_changelogs. Example metadata:
```
---
layout: changelog
title: Konsole 1.1.1 / KDE 3.0.1
sorted: 3001 # version for sorting
css-include: /css/main.css
---

* Feature 1
* Feature 2
```


### Blog

In `_config.yml`, put
```
paginate: 3
```

And in the `index.html` page, you can include the blog component.

```
{% include blog.html %}
```

### Options

Following options are available in `_config.yml`

* `use-kde-logo`: if you want to use the kde logo instead of an application icon
* `hide-title-nav`: To hide the site title besides logo in navigation bar
* `app_icon`: Path to the icon displayed in the navbar
* `donation`: Set `false` to hide donation form at the bottom of the website. `true` by default
* `navigation`: Navigation menu settings
  * `navigation.top[].title`: Title for the top navigation menu item
  * `navigation.top[].url`: URL for the top navigation menu item
  * `navigation.top[].blank`: URL open a new tab if set
  * `navigation.top[].components[].title`: Title for the Component
  * `navigation.top[].components[].url`: URL for the Component
  * `navigation.top[].components[].background`: Background Image URL for the Component
  * `navigation.bottom[].name`: Section title for the bottom links
    * `navigation.bottom[].items[].title`: Title for the bottom links
    * `navigation.bottom[].items[].url`: URL for the bottom links
* `maintainer`: Maintainer name and link shown at the bottom left of the pages
  * `maintainer.name`: Display name of the maintainer team. "KDE www" by default
  * `maintainer.href`: URL or email address link for the maintainer team. "mailto:kde-www@kde.org" by default
* `social`: Settings for social links shown at the bottom right of the pages. If you do not specify this parameter, default social links would be shown.
  * `social.facebook`: Link to the Facebook page. e.g. "https://www.facebook.com/kde/"
  * `social.twitter`: Link to the Twitter profile. e.g. "https://twitter.com/kdecommunity"
  * `social.diaspora`: Link to the Diaspora profile. e.g. "https://joindiaspora.com/people/9c3d1a454919ef06"
  * `social.mastodon`: Link to the Mastodon profile. e.g. "https://mastodon.technology/@kde"
  * `social.linkedin`: Link to the LinkedIn profile. e.g. "https://www.linkedin.com/company/29561/"
  * `social.reddit`: Link to the Reddit discussion page. e.g. "https://www.reddit.com/r/kde/"
  * `social.youtube`: Link to the YouTube channel. e.g. "https://www.youtube.com/channel/UCF3I1gf7GcbmAb0mR6vxkZQ"
  * `social.peertube`: Link to PeerTube channel. e.g. "https://peertube.mastodon.host/accounts/kde/videos"
  * `social.vk`: Link to VK profile. e.g. "https://vk.com/kde_ru"
* `favicon` add a favicon otherwise use kde logo
* `apple-itunes-app`: Settings for [Smart App Banners](https://developer.apple.com/documentation/webkit/promoting_apps_with_smart_app_banners) on WebKit-based browsers. This can also be configured for specific pages only.
  * `apple-itunes-app.app-id`: The app's unique identifier on the Apple App Store

`navigation` option example:

```yaml
navigation:
  top:
    - title: Changelog
      url: /changelog.html
    - title: Download
      url: /download.html
  bottom:
    - name: Community
      items:
        - title: Konsole Forums
          url: https://forum.kde.org/viewforum.php?f=227&sid=2e54340bf2c58589fec0f3406a4ce171
    - name: News &amp; Press
      items:
        - title: Announcements
          url: https://www.kde.org/announcements/
        - title: KDE.news
          url: https://dot.kde.org/
```

## Build example

```bash
cd example/simple
bundle install --path vendor/bundle
bundle exec jekyll serve
```

## List site using this theme

* konsole.kde.org
* cantor.kde.org
* kirogi.org
* fr.kde.org
* conf.kde.in
* kphotoalbum.org WIP
* kmymoney.org WIP

Please tell me if you create a new website using this theme. So that I can do the modification in case of breaking change.

## Contributing

Bug reports and pull requests are welcome on the KDE Gitlab at https://invent.kde.org/websites/jekyll-kde-theme.

## Development

To set up your environment to develop this theme, run `bundle install`.

Your theme is setup just like a normal Jekyll site! To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`, `_sass` and `assets` tracked with Git will be bundled.
To add a custom directory to your theme-gem, please edit the regexp in `jekyll-kde-theme.gemspec` accordingly.

## License

This program is licensed under the GNU AGPL. See [KDE Licensing policy](https://community.kde.org/Policies/Licensing_Policy).
